const Discord = require('discord.js');
const config = require("./config.json");

const bot = new Discord.Client();

bot.on('ready', () => {
	console.log('Ready');
});

bot.on("message", msg => {
 	if (!msg.content.startsWith(config.prefix)) {
		return;
	}

	if (msg.author.bot) {
		return;
	}

	if (msg.content.startsWith(config.prefix + "info")) {
		msg.channel.sendMessage(`${msg.author}\`\`\`
Info for ${config.botName}

Bot Creator: Oscar Wos
Bot Version: 1.0.1
\`\`\``);
 	} else if (msg.content.startsWith(config.prefix + "commands")) {
		msg.channel.sendMessage(`
${msg.author}\`\`\`
Commands for ${config.botName}

${config.prefix}info - View info of the bot
${config.prefix}commands - View the current commands

${config.prefix}music - View Music commands
\`\`\``);
	} else if (msg.content.startsWith(config.prefix + "music")) {
		if (msg.content.endsWith("music")) {
			msg.channel.sendMessage(`
${msg.author}\`\`\`
Music Commands for ${config.botName}

${config.prefix}music play - Play a song <url / Search Terms>
${config.prefix}music stop - Stop a current song (if playing)
\`\`\``);
		} else if (msg.content.startsWith("play", 7)) {
			if (msg.content.endsWith("play")) {
				msg.channel.sendMessage(`
${msg.author}\`\`\`
${config.prefix}music play <url / Search Terms>
\`\`\`
`);
			} else {
				if (msg.content.startsWith("http", 12)) {
					var url;

					url = msg.content.substring(12, msg.content.length);
					msg.channel.sendMessage(`${url}`);


				} else {
					var url;

					search = msg.content.substring(12, msg.content.length);
					msg.channel.sendMessage(`${search}`);
					//Search
				}
			}
		}
	}
});

bot.on("guildMemberAdd", (member) => {
	var User = member.user;

	User.sendMessage("Hi " + User.username);
	User.sendMessage("Welcome to TangoWorldWide");
});

bot.login(config.token);
